#include<stdio.h>
#include<time.h>
#include<string.h>
#include<stdlib.h>
#include<malloc.h>
#define FLAG_LENGTH 300
int stlen(char n[]);
char *compare(char x[]);
void *malloc_handler(int bytes);
int strcp(char cmp[], char flag[]);

int main(void)
{  
   char flag[FLAG_LENGTH];
   char *n = (char *)malloc_handler(70);
   n = getenv("USER");
   char *cmp = malloc_handler(FLAG_LENGTH);
   strcat(cmp, "Wait, your name is");
   strcat(cmp, n);
   printf("Enter your flag.\n");
   scanf("%s", &flag);
   char *s = compare(cmp);
   if (strcp(s, flag) == 0)
      printf("Done.\n");
   else
      printf("Try again.\n");
}

char *compare(char n[])
{
   int i, j, c, d;
   time_t raw;
   struct tm *timeinfo;
   time(&raw);
   timeinfo = localtime(&raw);
   for (i = c = j = d = 0; i < stlen(n); i++)
      n[i] ^= timeinfo->tm_min >> timeinfo->tm_mday ;
   return n;
}

int stlen(char n[])
{
   int i = 0;
   while (n[i] != '\0')
      i++;
   return i;
}

int strcp(char cmp[], char flag[])
{
   for (int i = 0; i < stlen(cmp); i++)
      if (cmp[i] != flag[i])
         return 1;
   return 0;
}

void *malloc_handler(int bytes)
{
   void *ptr;
   ptr = malloc(bytes);
   if (ptr == NULL)
      exit(EXIT_FAILURE);
   return ptr;
}